# MapLibre

This gitlab page centralizes all the html / css / javascript syntaxes for MapLibreGL-based maps accessible on this website https://sites-formations.univ-rennes2.fr/mastersigat/MaplibreGL


Maps codes are documented and commented on.

All the resources under available according to the license Creative Commons BY-NC-SA

